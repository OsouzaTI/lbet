package com.osouza.lbet.services

import com.osouza.lbet.models.League
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.ParameterizedTypeReference
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.stereotype.Service
import org.springframework.web.client.RestTemplate


@Service
class APIService {

    val ITEMS_URL = "https://ddragon.bangingheads.net/cdn/latest/img/item/"
    val CHAMPIONS_URL = "https://ddragon.bangingheads.net/cdn/latest/img/champion/"
    val API_URL_PERSISTED = "https://esports-api.lolesports.com/persisted/gw"
    val API_URL_LIVE = "https://feed.lolesports.com/livestats/v1"
    val API_KEY = "0TvQnueqKa5mxJntVWt0w4LpLfEkrV1Ta8rQBb9Z"

    @Autowired
    private lateinit var template : RestTemplate

    fun getAllLeaguesByRegion(region: String): List<League> {
        val headers = HttpHeaders()
        headers.set("x-api-key", API_KEY)

        val entity = HttpEntity<Void>(headers)
        val response = template.exchange(
            "$API_URL_PERSISTED/getLeagues?hl=$region",
            HttpMethod.GET,
            entity,
            object : ParameterizedTypeReference<List<League>>() {}).body
        println(response)
        return response ?: emptyList()
    }



}
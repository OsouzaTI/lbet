package com.osouza.lbet

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class LbetApplication

fun main(args: Array<String>) {
	runApplication<LbetApplication>(*args)
}

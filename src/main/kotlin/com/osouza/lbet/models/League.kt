package com.osouza.lbet.models

data class League(val id : String, val name : String, val image : String)

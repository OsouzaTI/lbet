package com.osouza.lbet.controllers

import com.osouza.lbet.models.League
import com.osouza.lbet.services.APIService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.client.RestTemplate
import org.springframework.web.client.getForObject




@Controller
@RequestMapping("/")
class IndexController {

    @Autowired
    private lateinit var apiService : APIService

    @GetMapping
    fun index() {

        apiService.getAllLeaguesByRegion("pt-BR")

    }


}